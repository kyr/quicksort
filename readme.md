#quicksort

implementation of the quicksort algorithm in js

```javascript
var qs = require('quicksort');
qs.quicksort([5, 3, 7, 5, 1], 0, 4, function (result) {
  console.log(result);
});
// -> [ 1, 3, 5, 5, 7 ]
```

if you want it sync:

```javascript
var qs = require('quicksort');
console.log(qs.quicksortSync([5, 3, 7, 5, 1], 0, 4));
// -> [ 1, 3, 5, 5, 7 ]
```