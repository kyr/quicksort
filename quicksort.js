module.exports = {
  quicksort: function (items, left, right, callback) {
    function swap(items, firstIndex, secondIndex){
      var temp = items[firstIndex];
      items[firstIndex] = items[secondIndex];
      items[secondIndex] = temp;
    }

    function partition (items, left, right) {
      var pivot = items[Math.floor((right + left) / 2)],
      i = left,
      j = right;

      while (i <= j) {
        while (items[i] < pivot) {
          i++;
        }

        while (items[j] > pivot) {
          j--;
        }

        if (i <= j) {
          swap(items, i, j);
          i++;
          j--;
        }
      }
      return i;
    }

    var index;
    if (items.length > 1) {
      index = partition(items, left, right);
      if (left < index - 1) {
        this.quicksort(items, left, index - 1, function (r) {});
      }

      if (index < right) {
        this.quicksort(items, index, right, function (r) {});
      }
    }
    callback(items);
  },

  quicksortSync: function (items, left, right) {
    function swap(items, firstIndex, secondIndex){
      var temp = items[firstIndex];
      items[firstIndex] = items[secondIndex];
      items[secondIndex] = temp;
    }

    function partition (items, left, right) {
      var pivot = items[Math.floor((right + left) / 2)],
      i = left,
      j = right;

      while (i <= j) {
        while (items[i] < pivot) {
          i++;
        }

        while (items[j] > pivot) {
          j--;
        }

        if (i <= j) {
          swap(items, i, j);
          i++;
          j--;
        }
      }
      return i;
    }

    var index;
    if (items.length > 1) {
      index = partition(items, left, right);
      if (left < index - 1) {
        this.quicksort(items, left, index - 1);
      }

      if (index < right) {
        this.quicksort(items, index, right);
      }
    }
    return items;
  }
}